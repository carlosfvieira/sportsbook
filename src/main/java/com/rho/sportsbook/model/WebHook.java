package com.rho.sportsbook.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.dizitart.no2.objects.Id;

import java.util.Date;
import java.util.UUID;

public class WebHook {

    @Id
    private String id; // uuid
    private String url;
    private Date ts;

    @JsonCreator
    public WebHook(String url) {
        this.id = UUID.randomUUID().toString();
        this.ts = new Date(System.currentTimeMillis());
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }
}
