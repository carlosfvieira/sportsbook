package com.rho.sportsbook.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.objects.Id;

import java.util.Date;
import java.util.UUID;

@Slf4j
public class Bet {

    @Id
    private String id;  // uuid
    private Date ts;
    private float stake;
    private String accountId;

    public Bet() {
        this.ts = new Date(System.currentTimeMillis());
        this.id = UUID.randomUUID().toString();
    }

    @JsonCreator
    public Bet(float stake, String accountId) {
        this.id = UUID.randomUUID().toString();
        this.ts = new Date(System.currentTimeMillis());
        this.stake = stake;
        this.accountId = accountId;
    }

    public Bet(Date ts, float stake, String accountId) {
        this.id = UUID.randomUUID().toString();
        this.ts = ts;
        this.stake = stake;
        this.accountId = accountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public float getStake() {
        return stake;
    }

    public void setStake(float stake) {
        this.stake = stake;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }
}
