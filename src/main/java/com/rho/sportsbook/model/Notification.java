package com.rho.sportsbook.model;

import org.dizitart.no2.objects.Id;

import java.util.Date;
import java.util.UUID;

public class Notification {

    @Id
    private String id; // uuid
    private Date ts;
    private Bet bet;
    private float flaggedValue;

    public Notification() {
        this.id = UUID.randomUUID().toString();
        this.ts = new Date(System.currentTimeMillis());
    }

    public Notification(Bet bet, float flaggedValue) {
        this.id = UUID.randomUUID().toString();
        this.ts = new Date(System.currentTimeMillis());
        this.bet = bet;
        this.flaggedValue = flaggedValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public float getFlaggedValue() {
        return flaggedValue;
    }

    public void setFlaggedValue(float flaggedValue) {
        this.flaggedValue = flaggedValue;
    }
}
