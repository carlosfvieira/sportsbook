package com.rho.sportsbook.dao;

import com.rho.sportsbook.database.EntityManager;
import com.rho.sportsbook.model.Bet;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class BetDAO implements Dao<Bet> {

    @Autowired
    EntityManager entityManager;

    @Override
    public Optional<Bet> get(String id) {
        return null;
    }

    @Override
    public List<Bet> getAll() {
        return entityManager.getBetStore().find().toList();
    }

    @Override
    public void save(Bet bet) {
        entityManager.getBetStore().insert(bet);
    }

    @Override
    public void update(Bet bet, String[] params) {

    }

    @Override
    public void delete(Bet bet) {

    }

    /**
     * Gets last bets given time window
     * @param timeWindow in seconds
     * @return list of bets within timeWindow
     */
    public List<Bet> getLastBets(int timeWindow) {
        Date start = new Date(System.currentTimeMillis() - timeWindow * 1000);
        List<Bet> list = entityManager.getBetStore().find(ObjectFilters.gt("ts", start)).toList();
        return list;
    }
}
