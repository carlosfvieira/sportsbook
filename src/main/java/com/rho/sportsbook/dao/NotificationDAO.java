package com.rho.sportsbook.dao;

import com.rho.sportsbook.database.EntityManager;
import com.rho.sportsbook.model.Notification;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class NotificationDAO implements Dao<Notification> {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<Notification> get(String id) {
        return Optional.empty();
    }

    @Override
    public List<Notification> getAll() {
        return entityManager.getNotificationStore().find().toList();
    }

    @Override
    public void save(Notification notification) {
        entityManager.getNotificationStore().insert(notification);
    }

    @Override
    public void update(Notification notification, String[] params) {

    }

    @Override
    public void delete(Notification notification) {

    }

    /**
     * Finds all notifications by accountId
     * @param accountId
     * @return list of notifications
     */
    public List<Notification> getAllByAccountId(String accountId) {
        return entityManager.getNotificationStore().find(ObjectFilters.eq("bet.accountId", accountId)).toList();
    }

}
