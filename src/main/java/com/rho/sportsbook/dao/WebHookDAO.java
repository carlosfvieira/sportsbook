package com.rho.sportsbook.dao;

import com.rho.sportsbook.database.EntityManager;
import com.rho.sportsbook.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.objects.ObjectFilter;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class WebHookDAO implements Dao<WebHook> {

    @Autowired
    EntityManager entityManager;

    @Override
    public Optional get(String id) {
        return Optional.empty();
    }

    @Override
    public List<WebHook> getAll() {
        return entityManager.getWebHookStore().find().toList();
    }

    @Override
    public void save(WebHook webHook) {
        entityManager.getWebHookStore().insert(webHook);
    }

    @Override
    public void update(WebHook webHook, String[] params) {

    }

    @Override
    public void delete(WebHook webHook) {
        entityManager.getWebHookStore().remove(webHook);
    }

    public Boolean deleteById(String id){
        WebHook webHook = entityManager.getWebHookStore().find(ObjectFilters.eq("id", id)).firstOrDefault();
        if (webHook == null){
            return false;
        }
        this.delete(webHook);
        return true;
    }
}
