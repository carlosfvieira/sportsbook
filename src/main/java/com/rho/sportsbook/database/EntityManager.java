package com.rho.sportsbook.database;

import com.rho.sportsbook.config.DatabaseConfig;
import com.rho.sportsbook.model.Bet;
import com.rho.sportsbook.model.Notification;
import com.rho.sportsbook.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class EntityManager {

    private Nitrite db;
    ObjectRepository<Bet> betStore;
    ObjectRepository<Notification> notificationStore;
    ObjectRepository<WebHook> webHookStore;

    @Autowired
    public EntityManager(DatabaseConfig config) {
        initDB(config);
        initRepositories();
    }

    private void initDB(DatabaseConfig config){
        log.info("Initializing database...");

        if (config.getPath().equals("")){
            log.info("No db.path property was specified, so database will be in-memory only database (nice for tests, for example)");
        }

        db = Nitrite.builder()
                .filePath(config.getPath())
                .openOrCreate(config.getUser(), config.getPassword());

        log.info("Database is ready");
    }

    private void initRepositories(){
        betStore = db.getRepository(Bet.class);
        log.info("Bet repository is ready");
        notificationStore = db.getRepository(Notification.class);
        log.info("Notification repository is ready");
        webHookStore = db.getRepository(WebHook.class);
        log.info("WebHook repository is ready");
    }

    public ObjectRepository<Bet> getBetStore() {
        return betStore;
    }

    public ObjectRepository<Notification> getNotificationStore() {
        return notificationStore;
    }

    public ObjectRepository<WebHook> getWebHookStore() {
        return webHookStore;
    }
}
