package com.rho.sportsbook.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value="classpath:application.properties", ignoreResourceNotFound = false)
@ConfigurationProperties(prefix="cep")
public class CEPConfig {

    private String runtimeuri;
    private int threshold = 100;      // 100£ by default
    private int timewindow = 60;      // 60s by default
    private int replaytimewindow = 0; // 0s by default

    public String getRuntimeuri() {
        return runtimeuri;
    }

    public void setRuntimeuri(String runtimeuri) {
        this.runtimeuri = runtimeuri;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getTimewindow() {
        return timewindow;
    }

    public void setTimewindow(int timewindow) {
        this.timewindow = timewindow;
    }

    public int getReplaytimewindow() {
        return replaytimewindow;
    }

    public void setReplaytimewindow(int replaytimewindow) {
        this.replaytimewindow = replaytimewindow;
    }
}
