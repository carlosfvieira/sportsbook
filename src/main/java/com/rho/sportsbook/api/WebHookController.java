package com.rho.sportsbook.api;

import com.rho.sportsbook.Service;
import com.rho.sportsbook.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
public class WebHookController {

    @Autowired
    Service service;

    @PostMapping("/webhook")
    ResponseEntity<WebHook> newWebHook(@RequestBody WebHook webHook){
        if (webHook.getUrl() == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        service.getWebHookDAO().save(webHook);
        log.info("New WebHook was posted. WebHook Id is " + webHook.getId());
        return ResponseEntity.ok(webHook);
    }

    @DeleteMapping("/webhook/{id}")
    ResponseEntity<WebHook> deleteWebHook(@PathVariable String id){
        if (service.getWebHookDAO().deleteById(id)) {
            log.info("WebHook with Id " + id + " was deleted");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.ok(null);
    }

}
