package com.rho.sportsbook.api;

import com.rho.sportsbook.Service;
import com.rho.sportsbook.cep.CEPEngine;
import com.rho.sportsbook.model.Bet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class BetController {

    @Autowired
    Service service;
    @Autowired
    CEPEngine cepEngine;

    @PostMapping("/bet")
    ResponseEntity<Bet> newBet(@RequestBody Bet bet){
        if (bet.getAccountId() == null || bet.getStake() == 0.0){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        service.getBetDAO().save(bet);
        log.info("New Bet was posted. Bet Id is " + bet.getId());
        cepEngine.sendEventBean(bet);
        return ResponseEntity.ok(bet);
    }
}