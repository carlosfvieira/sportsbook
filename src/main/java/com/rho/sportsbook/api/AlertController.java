package com.rho.sportsbook.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AlertController {

    @PostMapping("/alert")
    ResponseEntity<String> newAlert(@RequestBody Object obj) {
        log.info("New alert received from SportsBook service: " + obj.toString());
        return ResponseEntity.ok("{\"status\": \"OK\"}");
    }
}
