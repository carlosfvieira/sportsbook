package com.rho.sportsbook.events;

import com.rho.sportsbook.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class NotificationEventPublisher {
    @Autowired
    private ApplicationEventPublisher publisher;

    public void publishEvent(final Notification notification){
        NotificationEvent event = new NotificationEvent(this, notification);
        publisher.publishEvent(event);
    }
}
