package com.rho.sportsbook.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rho.sportsbook.Service;
import com.rho.sportsbook.model.Notification;
import com.rho.sportsbook.websocket.NotificationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class NotificationEventListener implements ApplicationListener<NotificationEvent> {

    @Autowired
    Service service;

    @Override
    public void onApplicationEvent(NotificationEvent notificationEvent) {
        log.info("New Notification Event");

        Notification notification = notificationEvent.getNotification();

        // Persist notification
        service.getNotificationDAO().save(notification);
        // Send notification info to STOMP session subscribers
        synchronized (service.getStompSession()) {
            service.getStompSession().send("/sportsbook/notify", new NotificationMessage(notification.getBet().getAccountId(), notification.getBet().getId(), notification.getFlaggedValue(), notification.getBet().getTs().toString()));
        }

        CompletableFuture.runAsync(() -> notifyHooks(notification));
    }

    private void notifyHooks(Notification notification) {
        service.getWebHookDAO().getAll().forEach(webHook -> {
            log.info("Send Notification info to webHook with URL: " + webHook.getUrl());
            URL url;
            HttpURLConnection con;
            try {
                url = new URL(webHook.getUrl());
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setConnectTimeout(5000);
                con.setRequestProperty("Content-Type", "application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonString = objectMapper.writeValueAsString(notification);
                log.info("Send this payload to client URL: " + jsonString);
                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonString.getBytes(StandardCharsets.UTF_8);
                    os.write(input, 0, input.length);
                }
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    log.info("Server response: " + response.toString());
                }
            } catch (ProtocolException e) {
                log.warn("Failed with ProtocolException: " + e.toString());
            } catch (MalformedURLException e) {
                log.warn("Failed with MalformedURLException: " + e.toString());
            } catch (IOException e) {
                log.warn("Failed with IOException: " + e.toString());
            }
        });
    }
}