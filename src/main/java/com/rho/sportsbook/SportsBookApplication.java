package com.rho.sportsbook;

import com.rho.sportsbook.cep.CEPEngine;
import com.rho.sportsbook.config.DatabaseConfig;
import com.rho.sportsbook.config.WSConfig;
import com.rho.sportsbook.database.EntityManager;
import com.rho.sportsbook.dao.BetDAO;
import com.rho.sportsbook.dao.NotificationDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {Service.class, BetDAO.class, NotificationDAO.class, EntityManager.class, DatabaseConfig.class, WSConfig.class, CEPEngine.class})
public class SportsBookApplication {

	public static void main(String[] args) { SpringApplication.run(SportsBookApplication.class, args); }

}
