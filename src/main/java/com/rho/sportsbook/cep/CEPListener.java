package com.rho.sportsbook.cep;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.internal.collection.Pair;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.UpdateListener;
import com.rho.sportsbook.Service;
import com.rho.sportsbook.model.Bet;
import com.rho.sportsbook.model.Notification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CEPListener implements UpdateListener {

    private final Service service;

    public CEPListener(Service service) {
        this.service = service;
    }

    @Override
    public void update(EventBean[] newEventBeans, EventBean[] oldEventBeans, EPStatement epStatement, EPRuntime epRuntime) {
        Bet flaggedBet = (Bet) ((Pair) newEventBeans[0].getUnderlying()).getFirst();
        log.info("NOTIFICATION: flagged bet id: " + flaggedBet.getId() + " | flagged account id: " + flaggedBet.getAccountId() + " | flagged amount: " + newEventBeans[0].get("flaggedAmount"));
        Notification notification = new Notification(flaggedBet, Float.parseFloat(newEventBeans[0].get("flaggedAmount").toString()));
        service.getNotificationEventPublisher().publishEvent(notification);
    }

}
