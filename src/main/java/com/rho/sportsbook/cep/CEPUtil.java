package com.rho.sportsbook.cep;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.DeploymentOptions;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPDeployment;
import com.espertech.esper.runtime.client.EPRuntime;
import com.rho.sportsbook.config.CEPConfig;
import com.rho.sportsbook.model.Bet;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CEPUtil {

    public static EPCompiled compileEPL(Configuration configuration, CEPConfig genericConfig) {
        // alert whenever there is an accumulated stake value > defined threshold (default 100£), on the defined time window (default 60s)
        // using externally timed window, based on the event timestamp and not runtime time... this is particularly useful, when recovering last events from persisted media, in the case of a service crash
        // Note: the use of ext_timed (to use event ts instead of runtime timestamp) requires that events should be sent ordered according to their timestamp
        String eplAlert = "select *, sum(stake) as flaggedAmount from Bet#ext_timed(ts.toMillisec(), " + genericConfig.getTimewindow() + " sec) group by accountId having sum(stake) > " + genericConfig.getThreshold();

        log.info("Compiling EPL statement");
        EPCompiled compiled;
        try {
            compiled = EPCompilerProvider.getCompiler().compile(eplAlert, new CompilerArguments(configuration));
            log.info("EPL statement compiled with success");
        } catch (EPCompileException ex) {
            throw new RuntimeException(ex);
        }

        return compiled;
    }
    
    public static Configuration getConfiguration() {
        Configuration configuration = new Configuration();
        configuration.getCommon().addEventType(Bet.class);
        return configuration;
    }

    public static EPDeployment deploy(EPRuntime runtime, EPCompiled compiled) {
        try {
            EPDeployment epd = runtime.getDeploymentService().deploy(compiled, new DeploymentOptions().setDeploymentId("bets"));
            log.info("EPL statement was deployed with success");
            return epd;
        } catch (EPDeployException ex) {
            log.error("Could not compile and deploy EPL Statement(s)");
            throw new RuntimeException(ex);
        }
    }
}
