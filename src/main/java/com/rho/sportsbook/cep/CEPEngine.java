package com.rho.sportsbook.cep;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.runtime.client.*;
import com.rho.sportsbook.Service;
import com.rho.sportsbook.config.CEPConfig;
import com.rho.sportsbook.model.Bet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class CEPEngine implements CommandLineRunner {

    private EPRuntime runtime;
    private EPCompiled compiled;
    CEPConfig config;
    Service service;
    
    @Autowired
    public CEPEngine(CEPConfig config, Service service) throws InterruptedException {
        initEngine(config, service);
    }

    private void initEngine(CEPConfig config, Service service) throws InterruptedException {
        this.config = config;
        this.service = service;
        log.info("Starting CEP engine...");
        Configuration configuration = CEPUtil.getConfiguration();
        configuration.getCommon().addEventType(Bet.class);
        compiled = CEPUtil.compileEPL(configuration, config);
        runtime = EPRuntimeProvider.getRuntime(config.getRuntimeuri(), configuration);
        EPDeployment deployment = CEPUtil.deploy(runtime, compiled);
        deployment.getStatements()[0].addListener(new CEPListener(service));
        log.info("CEP engine started");
    }

    /**
     * Redeploy statements and thus clearing all related events
     */
    public void reDeploy(){
        try {
            runtime.getDeploymentService().undeploy("bets");
        } catch (EPUndeployException e) {
            e.printStackTrace();
        }
        EPDeployment deployment = CEPUtil.deploy(runtime, compiled);
        deployment.getStatements()[0].addListener(new CEPListener(service));
    }

    private void loadOlderEvents(){
        if (config.getReplaytimewindow() > 0) {
            log.info("Get last bets using time window defined: " + config.getReplaytimewindow() + " seconds");
            List<Bet> bets = this.service.getBetDAO().getLastBets(config.getReplaytimewindow());
            bets.forEach(bet -> {
                this.sendEventBean(bet);
            });
        }else{
            log.info("No replay bets time window defined");
        }
    }

    public void sendEventBean(Bet bet){
        log.info("Send EventBean (Bet) to CEP Engine. Bet Id is: " + bet.getId() + " with timestamp: " + bet.getTs() + " with Account Id: " + bet.getAccountId() + " with stake: " + bet.getStake());
        this.runtime.getEventService().sendEventBean(bet, bet.getClass().getSimpleName());
    }

    @Override
    public void run(String... args) throws Exception {
        this.loadOlderEvents();
    }
}
