package com.rho.sportsbook;

import com.rho.sportsbook.config.WSConfig;
import com.rho.sportsbook.dao.WebHookDAO;
import com.rho.sportsbook.events.NotificationEventPublisher;
import com.rho.sportsbook.websocket.SSHandler;
import com.rho.sportsbook.dao.BetDAO;
import com.rho.sportsbook.dao.NotificationDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.concurrent.ExecutionException;

@Slf4j
@org.springframework.stereotype.Service
public class Service implements CommandLineRunner {

    @Autowired
    BetDAO betDAO;
    @Autowired
    NotificationDAO notificationDAO;
    @Autowired
    WebHookDAO webHookDAO;
    @Autowired
    WSConfig wsConfig;
    @Autowired
    NotificationEventPublisher notificationEventPublisher;

    private StompSession stompSession;

    public Service() throws ExecutionException, InterruptedException {
        log.info("Service");
    }

    public StompSession getStompSession() {
        return stompSession;
    }

    public BetDAO getBetDAO() {
        return betDAO;
    }

    public void setBetDAO(BetDAO betDAO) {
        this.betDAO = betDAO;
    }

    public NotificationDAO getNotificationDAO() {
        return notificationDAO;
    }

    public WebHookDAO getWebHookDAO() {
        return webHookDAO;
    }

    public void setWebHookDAO(WebHookDAO webHookDAO) {
        this.webHookDAO = webHookDAO;
    }

    public NotificationEventPublisher getNotificationEventPublisher() {
        return notificationEventPublisher;
    }

    public void setNotificationDAO(NotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    private void initStompSession() throws ExecutionException, InterruptedException {
        WebSocketClient client = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        SSHandler sessionHandler = new SSHandler();
        stompSession = stompClient.connect(wsConfig.getUrl(), sessionHandler).get();
    }

    @Override
    public void run(String... args) throws Exception {
        initStompSession();
    }
}
