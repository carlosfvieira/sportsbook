package com.rho.sportsbook.websocket;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NotificationMessage {

    private String message;
    private String accountId;
    private String betId;
    private float flaggedValue;
    private String ts;

    public NotificationMessage() {
    }

    public NotificationMessage(String accountId, String betId, float flaggedValue, String betTs) {
        this.accountId = accountId;
        this.betId = betId;
        this.flaggedValue = flaggedValue;
        this.ts = betTs;
        this.message = "[BET TS] " + betTs +  " [BET_ID] " + betId + " [ACCOUNT_ID] " + accountId + " [FLAGGED_AMOUNT] " + flaggedValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBetId() {
        return betId;
    }

    public void setBetId(String id) {
        this.betId = id;
    }

    public float getFlaggedValue() {
        return flaggedValue;
    }

    public void setFlaggedValue(float flaggedValue) {
        this.flaggedValue = flaggedValue;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
