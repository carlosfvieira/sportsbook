package com.rho.sportsbook.websocket;

import com.rho.sportsbook.model.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Slf4j
@Controller
public class NotificationController {

    @MessageMapping("/notify")
    @SendTo("/alert/notifications")
    public Alert notify(NotificationMessage notification){
        log.info("Message to STOMP session subscribers: " + notification.getMessage());
        return new Alert(notification.getMessage());
    }
}
