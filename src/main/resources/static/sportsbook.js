var stompClient = null;
var socket = null;
var interval = null;

function connect() {
    stompClient = Stomp.over(new SockJS('/websocket'));
    stompClient.reconnect_delay = 5000;
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/alert/notifications', function (data) {
            showAlert(JSON.parse(data.body).message);
        });
        if (interval !== null){
            clearInterval(interval);
            interval = null;
        }
    }, function(){
        disconnect();
        if (interval == null){
            interval = setInterval(connect, 1000);
        }
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
        stompClient = null;
        console.log("Disconnected");
    }
}

function showAlert(message) {
    console.log("showAlert: " + message);
    $("#alerts").append("<tr><td>" + message + "</td></tr>");
}

connect();