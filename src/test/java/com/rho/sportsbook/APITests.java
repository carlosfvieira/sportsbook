package com.rho.sportsbook;

import com.rho.sportsbook.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = SportsBookApplication.class
)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
class APITests {

    @Autowired
    Service service;
    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void betController(){
        webTestClient // missing stake value
                .post()
                .uri("/bet")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue("{\"AccountId\": \"" + UUID.randomUUID().toString() + "\"}"))
                .exchange()
                .expectStatus()
                .isBadRequest();

        webTestClient
                .post()
                .uri("/bet")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue("{\"stake\": 50, \"accountId\":\"" + UUID.randomUUID().toString() + "\"}"))
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    public void webHpookController(){
        WebHook webhook = new WebHook("http://some.url.here");
        service.getWebHookDAO().save(webhook);

        webTestClient
                .post()
                .uri("/webhook")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue("{\"invalidkey\": \"http://some.url.here\"}"))
                .exchange()
                .expectStatus()
                .isBadRequest();

        webTestClient
                .post()
                .uri("/webhook")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue("{\"url\": \"http://some.url.here\"}"))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(WebHook.class);

        webTestClient
                .delete()
                .uri("/webhook/{id}", webhook.getId())
                .exchange()
                .expectStatus()
                .isOk();
    }

}
