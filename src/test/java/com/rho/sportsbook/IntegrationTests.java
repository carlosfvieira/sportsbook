package com.rho.sportsbook;

import com.rho.sportsbook.cep.CEPEngine;
import com.rho.sportsbook.model.Bet;
import com.rho.sportsbook.model.Notification;
import com.rho.sportsbook.websocket.NotificationMessage;
import com.rho.sportsbook.websocket.SSHandler;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.*;

import static org.awaitility.Awaitility.await;

/**
 * Test the basic workflow of receiving Bet requests and get them evaluated by the CEP Engine, which tests:
 * - API: posting Bets
 * - DB: checking if notifications are being persisted
 * - WebSockets: tested checking if notification messages are being sent through and received by subscribers
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = SportsBookApplication.class
)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
class IntegrationTests {

    @Autowired
    CEPEngine cepEngine;
    @Autowired
    Service service;
    @Autowired
    private WebTestClient webTestClient;

    public boolean checkDBNotificationsByAccount(String accountId){
        List<Notification> list = service.getNotificationDAO().getAllByAccountId(accountId);

        list.forEach(notification -> log.info(notification.getBet().getAccountId()));

        Float[] expectedFlaggedAmounts = {101f, 102f, 110f};
        log.info(String.valueOf(list.size()));
        Assertions.assertEquals(3, list.size()); // Account1 should have been notified 3 times
        list.forEach(notification -> Assertions.assertTrue(Arrays.asList(expectedFlaggedAmounts).contains(notification.getFlaggedValue())));
        return true;
    }

    public boolean assertWebSocketMessages(BlockingQueue<NotificationMessage> webSocketBlockingQueue) {
        log.info(String.valueOf(webSocketBlockingQueue.size()));
        Assertions.assertEquals(3, webSocketBlockingQueue.size());
        return true;
    }

    public BlockingQueue<NotificationMessage> getWebSocketBlockingQueue() {
        BlockingQueue<NotificationMessage> blockingQueue = new ArrayBlockingQueue<>(100);

        service.getStompSession().subscribe("/alert/notifications", new SSHandler() {

            @Override
            public Type getPayloadType(StompHeaders headers) {
                return NotificationMessage.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                blockingQueue.add((NotificationMessage) payload);
            }

        });

        return blockingQueue;
    }

    /**
     * Given a known scenario, test if CEP engine correctly evaluates Bets and flags accounts
     * On this test, a batch like scenario is used, where a set of Bets are "loaded" initially, but the CEP Engine needs to evaluated them correctly based on their timestamp, and not entry timestamp
     * Bet events are sent directly to the engine
     */
    @Test
    public void flaggedAccountsBatchEvents() {
        cepEngine.reDeploy();

        String accountId1 = UUID.randomUUID().toString();
        String accountId2 = UUID.randomUUID().toString();
        Calendar cal = Calendar.getInstance();
        long timeInMillis = cal.getTimeInMillis();
        BlockingQueue<NotificationMessage> webSocketBlockingQueue = getWebSocketBlockingQueue();

        cepEngine.sendEventBean(new Bet(new Date(timeInMillis), 50f, accountId1));
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 38000), 40f, accountId1)); // +38s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 70000), 50f, accountId1)); // +70s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 90000), 11f, accountId1)); // +90s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 100000), 99f, accountId2)); // +110s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 110000), 41f, accountId1)); // +110s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 172000), 70f, accountId1)); // +172s
        cepEngine.sendEventBean(new Bet(new Date(timeInMillis + 197000), 40f, accountId1)); // +197s

        await().until( () -> this.assertWebSocketMessages(webSocketBlockingQueue));
        await().until( () -> this.checkDBNotificationsByAccount(accountId1));
    }

    /**
     * Given a known scenario, test if CEP engine correctly evaluates Bets and flags accounts
     * On this test, a normal workflow is tested, replicating a normal operation of the service, but Bet events are sent directly to the engine
     */
    @Test
    public void flaggedAccountsNormalFlowEvents() throws InterruptedException {
        cepEngine.reDeploy();

        String accountId = UUID.randomUUID().toString();
        BlockingQueue<NotificationMessage> webSocketBlockingQueue = getWebSocketBlockingQueue();

        cepEngine.sendEventBean(new Bet(new Date(), 50f, accountId));
        Thread.sleep(38000);
        cepEngine.sendEventBean(new Bet(new Date(), 40f, accountId));
        Thread.sleep(32000);
        cepEngine.sendEventBean(new Bet(new Date(), 50f, accountId));
        Thread.sleep(20000);
        cepEngine.sendEventBean(new Bet(new Date(), 11f, accountId));
        Thread.sleep(20000);
        cepEngine.sendEventBean(new Bet(new Date(), 41f, accountId));
        Thread.sleep(62000);
        cepEngine.sendEventBean(new Bet(new Date(), 70f, accountId));
        Thread.sleep(25000);
        cepEngine.sendEventBean(new Bet(new Date(), 40f, accountId));

        await().until( () -> this.assertWebSocketMessages(webSocketBlockingQueue));
        await().until( () -> this.checkDBNotificationsByAccount(accountId));
    }

    /**
     * Given a known scenario, test if CEP engine correctly evaluates Bets and flags accounts
     * On this test, a normal workflow is tested placing Bet events through the POST /bet endpoint
     */
    @Test
    public void flaggedAccountsNormalFlowEventsUsingAPI() throws InterruptedException {
        cepEngine.reDeploy();

        String accountId = UUID.randomUUID().toString();
        BlockingQueue<NotificationMessage> webSocketBlockingQueue = getWebSocketBlockingQueue();

        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 49, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(38000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 40, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(32000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 50, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(20000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 11, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(20000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 41, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(62000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 70, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();
        Thread.sleep(25000);
        webTestClient .post().uri("/bet").contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue("{\"stake\": 40, \"accountId\":" + "\"" + accountId + "\"" + "}")).exchange().expectStatus().isOk().expectBody();

        await().until( () -> this.assertWebSocketMessages(webSocketBlockingQueue));
        await().until( () -> this.checkDBNotificationsByAccount(accountId)); // test if expected notifications are in DB

    }

}
