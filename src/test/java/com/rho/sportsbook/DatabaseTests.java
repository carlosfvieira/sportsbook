package com.rho.sportsbook;

import com.rho.sportsbook.model.Bet;
import com.rho.sportsbook.model.Notification;
import com.rho.sportsbook.model.WebHook;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.UUID;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = SportsBookApplication.class
)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
class DatabaseTests {

    @Autowired
    private Service service;
    final int batchSize = 1000;

    @Test
    public void createBetsBatch(){
        int howMany = service.getBetDAO().getAll().size();

        for (int i = 0; i < batchSize ; i++) {
            service.getBetDAO().save(new Bet(new Random().nextInt(2000), UUID.randomUUID().toString()));
        }
        Assertions.assertTrue((batchSize == service.getBetDAO().getAll().size() - howMany));
    }

    @Test
    public void createNotificationsBatch(){
        int howMany = service.getNotificationDAO().getAll().size();

        for (int i = 0; i < batchSize ; i++) {
            service.getNotificationDAO().save(new Notification(new Bet(new Random().nextInt(2000), UUID.randomUUID().toString()), new Random().nextInt()));
        }
        Assertions.assertTrue((batchSize == service.getNotificationDAO().getAll().size() - howMany));
    }

    @Test
    public void createWebhooksBatch(){
        int howMany = service.getWebHookDAO().getAll().size();

        for (int i = 0; i < batchSize ; i++) {
            service.getWebHookDAO().save(new WebHook("http://some.url.here"));
        }
        Assertions.assertTrue((batchSize == service.getWebHookDAO().getAll().size() - howMany));
    }

}
